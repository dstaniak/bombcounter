#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QContextMenuEvent>
#include <QDateTime>
#include <QMainWindow>
#include <QMenu>
#include <QTimer>
#include <chrono>
#include "stdaliases.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

protected:
    void contextMenuEvent(QContextMenuEvent *event) override;
    void resizeEvent(QResizeEvent* event) override;
    void showEvent(QShowEvent* event) override;

private:
    Ui::MainWindow *ui;
    double baseDigitSize;
    double baseLabelFontSize;
    QTimer timer;
    QMenu contextMenu;
    QDateTime countdownEnd;
    QColor textColor;

    double currentDigitSize();
    QString getTextStyleSheet();
    void onMnuSelectColor();
    void onMnuSetTime();
    void onTimer();
    void updateColonsSizes();
    void updateTextStyleSheet();
};

#endif // MAINWINDOW_H
