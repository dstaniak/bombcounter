#ifndef STDALIASES_H
#define STDALIASES_H

#include <chrono>

typedef std::chrono::duration <double> Seconds;
typedef std::chrono::time_point<std::chrono::system_clock, Seconds> Timestamp;

#endif // STDALIASES_H
