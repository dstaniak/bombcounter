#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settimedialog.h"
#include <cmath>
#include <iostream>
#include <QColorDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    timer(this),
    contextMenu(this),
    textColor{115, 210, 22}
{
    ui->setupUi(this);
    contextMenu.addAction("Select color", this, &MainWindow::onMnuSelectColor);
    contextMenu.addAction("Set time", this, &MainWindow::onMnuSetTime);
    contextMenu.setStyleSheet(
                "QMenu{background:DimGrey}\n"
                "QMenu::item:selected{color: white;}\n"
                "QMenu::item{color: silver;}");
    baseDigitSize = 0;
    baseLabelFontSize = ui->lblHourColon->font().pointSize();
    connect(&timer, &QTimer::timeout, this, &MainWindow::onTimer);
    timer.start(1000);
    countdownEnd = QDateTime::currentDateTime();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
    contextMenu.popup(event->globalPos());
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    event->accept();
    updateColonsSizes();
}

void MainWindow::showEvent(QShowEvent *event)
{
    event->accept();
    if(baseDigitSize <= 0)
    {
        baseDigitSize = currentDigitSize();
        updateColonsSizes();
    }
}

double MainWindow::currentDigitSize()
{
    return ui->lcdHour1->geometry().width();
}

QString MainWindow::getTextStyleSheet()
{
    int r, g, b;
    textColor.getRgb(&r, &g, &b);
    return QString::asprintf("color: rgb(%d, %d, %d);", r, g, b);
}

void MainWindow::onMnuSelectColor()
{
    QColor newColor = QColorDialog::getColor(textColor, this, "Select digits color...");
    if(newColor.isValid())
    {
        textColor = newColor;
        updateTextStyleSheet();
    }
}

void MainWindow::onMnuSetTime()
{
    SetTimeDialog dialog;
    if(dialog.exec() == static_cast<int>(SetTimeDialog::Result::OK))
    {
        countdownEnd = dialog.datetime;
    } else {
        countdownEnd = QDateTime::currentDateTime();
    }
}

void MainWindow::onTimer()
{
    QDateTime now = QDateTime::currentDateTime();
    if(now >= countdownEnd) return;
    qint64 secs_to = now.secsTo(countdownEnd);
    qint64 hours_to = secs_to / 3600;
    qint64 mins_to = (secs_to / 60) % 60;
    secs_to = secs_to % 60;
    ui->lcdHour1->display(static_cast<int>(hours_to / 10));
    ui->lcdHour2->display(static_cast<int>(hours_to % 10));
    ui->lcdMinute1->display(static_cast<int>(mins_to / 10));
    ui->lcdMinute2->display(static_cast<int>(mins_to % 10));
    ui->lcdSecond1->display(static_cast<int>(secs_to / 10));
    ui->lcdSecond2->display(static_cast<int>(secs_to % 10));
}

void MainWindow::updateColonsSizes()
{
    int labelFontSize = static_cast<int>(std::round(baseLabelFontSize * (currentDigitSize() / baseDigitSize)));
    QFont font = ui->lblHourColon->font();
    font.setPixelSize(labelFontSize);
    ui->lblHourColon->setFont(font);
    ui->lblMinuteColon->setFont(font);
}

void MainWindow::updateTextStyleSheet()
{
    QString textStyleSheet = getTextStyleSheet();
    ui->lcdHour1->setStyleSheet(textStyleSheet);
    ui->lcdHour2->setStyleSheet(textStyleSheet);
    ui->lblHourColon->setStyleSheet(textStyleSheet);
    ui->lcdMinute1->setStyleSheet(textStyleSheet);
    ui->lcdMinute2->setStyleSheet(textStyleSheet);
    ui->lblMinuteColon->setStyleSheet(textStyleSheet);
    ui->lcdSecond1->setStyleSheet(textStyleSheet);
    ui->lcdSecond2->setStyleSheet(textStyleSheet);
}
