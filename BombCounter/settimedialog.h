#ifndef SETTIMEDIALOG_H
#define SETTIMEDIALOG_H

#include <QAbstractButton>
#include <QDateTime>
#include <QDialog>
#include "stdaliases.h"

namespace Ui {
class SetTimeDialog;
}

class SetTimeDialog : public QDialog
{
    Q_OBJECT

public:
    QDateTime datetime;

    explicit SetTimeDialog(QWidget *parent = nullptr);
    ~SetTimeDialog();

    enum class Result { OK, CANCEL };

public slots:

    void OnCountdownChanged(QDateTime datetime);
    void OnPointInTimeChanged(QDateTime datetime);

private:
    Ui::SetTimeDialog *ui;

    void OnOkClicked();
    void OnCancelClicked();
    void DoneWithCountdown();
    void DoneWithPointInTime();
};

#endif // SETTIMEDIALOG_H
