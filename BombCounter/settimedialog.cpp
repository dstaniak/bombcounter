#include "settimedialog.h"
#include "ui_settimedialog.h"
#include <chrono>

SetTimeDialog::SetTimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetTimeDialog)
{
    ui->setupUi(this);
    ui->tmePoint->setDateTime(QDateTime::currentDateTime());
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &SetTimeDialog::OnOkClicked);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &SetTimeDialog::OnCancelClicked);
    connect(ui->tmeValue, &QDateTimeEdit::dateTimeChanged, this, &SetTimeDialog::OnCountdownChanged);
    connect(ui->tmePoint, &QDateTimeEdit::dateTimeChanged, this, &SetTimeDialog::OnPointInTimeChanged);
}

SetTimeDialog::~SetTimeDialog()
{
    delete ui;
}

void SetTimeDialog::OnCountdownChanged(QDateTime datetime)
{
    Q_UNUSED(datetime)
    ui->chkCountdown->setChecked(true);
}

void SetTimeDialog::OnPointInTimeChanged(QDateTime datetime)
{
    Q_UNUSED(datetime)
    ui->chkPoint->setChecked(true);
}

void SetTimeDialog::OnOkClicked()
{
    if(ui->chkCountdown->isChecked()) DoneWithCountdown();
    if(ui->chkPoint->isChecked()) DoneWithPointInTime();
}

void SetTimeDialog::OnCancelClicked()
{
    done(static_cast<int>(Result::CANCEL));
}

void SetTimeDialog::DoneWithCountdown()
{
    datetime = QDateTime::currentDateTime();
    QTime time = ui->tmeValue->dateTime().time();
    int seconds = time.second();
    int minute_seconds = time.minute() * 60;
    int hour_seconds = time.hour() * 3600;
    datetime = datetime.addSecs(seconds + minute_seconds + hour_seconds);
    done(static_cast<int>(Result::OK));
}

void SetTimeDialog::DoneWithPointInTime()
{
    datetime = ui->tmePoint->dateTime();
    done(static_cast<int>(Result::OK));
}
